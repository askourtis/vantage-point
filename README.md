# Vantage Point Tree

## Short Description
A school project.

Implementing a shared memory vantage point tree construction, given a data set of d-dimensional points.

## Execution time comparisons

<img src="./graphs/s107d64.png" alt="Time comparison" width="400"/>
<img src="./graphs/s105d64.png" alt="Time comparison" width="400"/>
<img src="./graphs/s105d16.png" alt="Time comparison" width="400"/>
<img src="./graphs/s100d2.png" alt="Time comparison" width="400"/>

As shown, for many points and/or dimensions the execution time is drastically reduced with the use of threads, but for few points the overhead of creating these threads is much greater than the solution of the problem itself.

## Explaining why OpenMP is faster

OpenMP does not create and destroy the threads when they are not needed. Instead it just creates the threads when the binary is executed (or when the first parallel region is encountered), and it just reassigns the workload as needed. In the end only N original threads are created and destroyed at the end of the lifetime of the program. In comparison with the pThreads implementation, where a thread is created when needed but also destroyed at the end of the function.

This is also the reason why pThreads is faster than OpenMP when there are few points, since no threads are created in advance.

## Conclusion
- Threads yield better execution times only when the size of the problem is large enough.
- Pre-creation of threads leads to constant overhead, that yields better execution time than the dynamic-creation approach.
- There is a linear growth of the execution time in comparison to both the number of dimensions and the number of points, although the number of points has a much greater impact.
- Using OpenMP or Cilk is way easier than using pThreads, although what you gain in comfort, you lose in control.

## System Specifications and Endnotes
### The parallel implementation:
- Allocates 8 threads <sub><sup>(Excluding Cilk implementation)</sup></sub>
- Executes in parallel only when the problem size is larger than 10<sup>5</sup>. <sub><sup>(Problem size is the product of the number of points and the number of dimensions of a point)</sup></sub>

### System specifications:
CPU: Intel Core i7-3770
- Quad Core <sub><sup>(Supports hyper-threading)</sup></sub>
- 64 bit
- 8 MB L2 cache
- 3.4 GHz