#----------------VARIABLES----------------#
CC = gcc-5
HEADER_DIR := $(shell readlink -f ./inc)
SOURCE_DIR := $(shell readlink -f ./src)
BUILD_DIR  := $(shell readlink -f ./build)
LIB_DIR    := $(shell readlink -f ./lib)
MAIN       := $(shell readlink -f ./main.c)

FLAGS  = -Wall -I$(HEADER_DIR) -lm
DFLAGS = -g3
BFLAGS = -O3 -D NDEBUG
export

#----------------TARGETS----------------#
JOBS = compile build debug
.PHONY: $(JOBS)
$(JOBS): clean
	@$(MAKE) -C $(SOURCE_DIR) $@ TARGET=$(TARGET)

.PHONY: lib
lib: purge
	@for TARGET in sequential pthreads openmp cilk ; do \
        $(MAKE) -C $(SOURCE_DIR) lib TARGET=$$TARGET ; \
    done


#----------------HELPERS----------------#
.PHONY: clean
clean:
	rm -f $(BUILD_DIR)/$(TARGET)/*.o $(BUILD_DIR)/*.o
	rm -f $(LIB_DIR)/*

.PHONY: purge
purge: clean
	rm -rf $(BUILD_DIR)/$(TARGET)/* $(BUILD_DIR)/*.o
#----------------EOF----------------#