#ifndef PTHREADS_H_GUARD
#define PTHREADS_H_GUARD

//------------Includes------------//
#include <types.h>
#include <stdio.h>
#include <pthread.h>

//------------Macros------------//
//Error message when malloc/calloc returns NULL
#define MEM_ERR_MESSAGE ("System run out of memory.")
//Error message when a pThread cant be created
#define PT_ERR_MESSAGE ("Could not create thread.")
//Error message when a mutex object cant be initialized
#define MUT_ERR_MESSAGE ("Could not create mutex object.")

//------------Structs------------//
//The parameters for the build_tree
typedef struct params {
    Node* root;
    pthread_mutex_t* lock;
} Params;

//------------Declarations------------//
/* Recursively allocate nodes for a tree.
 *
 * Arguments:
 *  - void* root:
 *      The starting node of the tree [cast to Params*](should be allocated in heap).
 */
void* build_tree(void* arg);

#endif
//------------EOF------------//