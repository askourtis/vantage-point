#ifndef ALGORITHMS_H_GUARD
#define ALGORITHMS_H_GUARD

//------------Includes------------//
#include <stdlib.h>
#include <types.h>

//------------Macros------------//
//Prints an error message 'M', and terminates with error code 1
#define throw(M) {fprintf(stderr, M); exit(1);}

//------------Declarations------------//
/*
 * Partitions the distances array and the data array and returns the kth smallest element
 *
 * Arguments:
 *  - Data data
 *      The data to partition.
 *  - double* distances
 *      The distances to partition.
 *  - Index left
 *      The begin of the arrays.
 *  - Index right
 *      The end of the array.
 *  - Index k
 *      The index of the return value
 *
 * Returns:
 *  The kth smallest element based on the distance array
 */
double quick_select(const Data data, double* const distances, const Index left, const Index right, const Index k);

/*
 * Calculates the next power of two, given an input
 *
 * Arguments:
 *  - Size n
 *      A non-negative integer
 *
 * Returns:
 *  The next power of two that is greater than the argument n
 */
Size next_power_of_two(const Size n);

#endif
//------------EOF------------//