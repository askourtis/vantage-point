#ifndef VPTREE_H_GUARD
#define VPTREE_H_GUARD

typedef struct vpnode vptree;

vptree* buildvp(double* coords, int size, int dim);

vptree* getInner(vptree* root);

vptree* getOuter(vptree* root);

double getMD(vptree* root);

double* getVP(vptree* root);

int getIDX(vptree* node);

#endif