#ifndef TYPES_H_GUARD
#define TYPES_H_GUARD
//------------Includes------------//
#include <stddef.h>

//------------Macros------------//
//Calculates the index on an 1-dimensional array
#define IDX(i, j, d) ((i)*(d)+(j))

//Index type
typedef size_t Index;
//Size type
typedef size_t Size;

//------------Structs------------//
//A struct to store the data of a vpnode
typedef struct data {
    double* vantage;
    double* points;
    Size size;
    Size dim;
} Data;

//A struct to build the tree (vptree)
typedef struct vpnode {
    Index index;
    double value;
    Data data;
} Node;

#endif
//------------EOF------------//