#ifndef SEQUENTIAL_H_GUARD
#define SEQUENTIAL_H_GUARD

//------------Includes------------//
#include <types.h>
#include <stdio.h>

//------------Macros------------//
//Error message when malloc/calloc returns NULL
#define MEM_ERR_MESSAGE ("System run out of memory.")

//------------Declarations------------//
/* Recursively allocate nodes for a tree.
 *
 * Arguments:
 *  - Node* root:
 *      The starting node of the tree (should be allocated in heap).
 */
void build_tree(Node* root);

#endif
//------------EOF------------//