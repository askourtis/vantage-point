#include <cilk.h>
#include <math.h>
#include <algorithms.h>
#include <assert.h>

#define LEAST_SIZE 1000000

double* distances = NULL;
Size d_size = 0;

//-----------------Declarations-----------------//
double kth_nearest(Node* root, const Index k);

//-----------------Definitions-----------------//
void build_tree(Node* root) {
    assert(root != NULL);

    //Unpack needed variables
    Data data = root->data;

    if(data.size == 0)
        return;

    if(root->index == 0) {
        d_size = next_power_of_two(data.size - 1);
        distances = (double*)malloc(d_size*sizeof(double));
        if(distances == NULL)
            throw(MEM_ERR_MESSAGE);
    }

    //Index of the mid point (point with median distance from vantage)
    Index k = data.size/2 + data.size%2 - 1;
    root->value = kth_nearest(root, k);

    Size inner_size = k + 1;
    Size outer_size = data.size - inner_size;

    //No if since the inner_size is always greater than 0
    {
        double* const points = data.points;
        Node* inner = root+(root->index+1);
        *inner=(Node){.index=2*root->index+1,
                      .value  = -1.0,
                      .data=(Data){.vantage = points+(IDX(inner_size-1, 0, data.dim)),
                                   .points  = points,
                                   .size    = inner_size-1,
                                   .dim     = data.dim}};
        if(inner_size * data.dim > LEAST_SIZE)
            cilk_spawn build_tree(inner);
        else
            build_tree(inner);
    }

    if(outer_size) {
        double* const points = data.points + IDX(inner_size, 0, data.dim);
        Node* outer = root+(root->index+2);
        *outer=(Node){.index=2*root->index+2,
                      .value  = -1.0,
                      .data=(Data){.vantage = points+(IDX(outer_size-1, 0, data.dim)),
                                   .points  = points,
                                   .size    = outer_size-1,
                                   .dim     = data.dim}};
        build_tree(outer);
    }
    cilk_sync;
    if(root->index == 0)
        free(distances);
}

//-----------------Helper Definitions-----------------//
double* select_distance_array(Index index) {
    Size div = 1 << (int)log2(index+1);
    return distances+(((index+1) % div) * (d_size/div));
}

double kth_nearest(Node* root, const Index k) {
    const Data data = root->data;
    double* local = select_distance_array(root->index);

    //Ok this cilk_for is easy!!!
    cilk_for (Index i=0; i<data.size; ++i) {
        double value = 0.0;
        for(Index j=0; j<data.dim; ++j) {
            double xj = data.points[IDX(i,j,data.dim)] - data.vantage[j];
            value += xj*xj;
        }
        local[i] = sqrt(value);
    }
    return quick_select(data, local, 0, data.size-1, k);
}
//-----------------EOF-----------------//