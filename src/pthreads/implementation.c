#include <pthreads.h>
#include <math.h>
#include <algorithms.h>
#include <assert.h>
#include <stdio.h>

#define MAX_THREADS 8
#define LEAST_SIZE 1000000

volatile Size thread_counter = 0;
double* distances = NULL;
Size d_size = 0;

//-----------------Declarations-----------------//
double kth_nearest(Node* root, const Index k, pthread_mutex_t* lock);

//-----------------Definitions-----------------//
void* build_tree(void* arg) {
    assert(arg != NULL);

    Params* params = (Params*)arg;
    Node* root = params->root;
    pthread_mutex_t* lock = params->lock;

    assert(root != NULL);
    assert(lock != NULL);

    //Unpack needed variables
    Data data = root->data;

    if(data.size == 0)
        return NULL;

    if(root->index == 0) {
        d_size = next_power_of_two(data.size - 1);
        distances = (double*)malloc(d_size*sizeof(double));
        if(distances == NULL)
            throw(MEM_ERR_MESSAGE);
    }

    //Index of the mid point (point with median distance from vantage)
    Index k = data.size/2 + data.size%2 - 1;
    root->value = kth_nearest(root, k, lock);

    Size inner_size = k + 1;
    Size outer_size = data.size - inner_size;

    Params inner_params = (Params){.lock=lock};
    Params outer_params = (Params){.lock=lock};

    pthread_t id = pthread_self();
    //No if since the inner_size is always greater than 0
    {
        double* const points = data.points;
        Node* inner = root+(root->index+1);
        *inner=(Node){.index=2*root->index+1,
                      .value  = -1.0,
                      .data=(Data){.vantage = points+(IDX(inner_size-1, 0, data.dim)),
                                   .points  = points,
                                   .size    = inner_size-1,
                                   .dim     = data.dim}};
        inner_params.root = inner;

        if(inner_size * data.dim > LEAST_SIZE) {
            pthread_mutex_lock(lock);
            if(thread_counter < MAX_THREADS) {
                if(pthread_create(&id, NULL, build_tree, (void*)&inner_params) == 0)
                    ++thread_counter;
                else
                    throw(PT_ERR_MESSAGE);
            }
            pthread_mutex_unlock(lock);
        }

        if(id == pthread_self())
            build_tree((void*)&inner_params);
    }

    if(outer_size > 0) {
        double* const points = data.points + IDX(inner_size, 0, data.dim);
        Node* outer = root+(root->index+2);
        *outer=(Node){.index=2*root->index+2,
                      .value  = -1.0,
                      .data=(Data){.vantage = points+(IDX(outer_size-1, 0, data.dim)),
                                   .points  = points,
                                   .size    = outer_size-1,
                                   .dim     = data.dim}};
        outer_params.root = outer;
        build_tree((void*)&outer_params);
    }

    if(id != pthread_self()){
        pthread_join(id, NULL);
        pthread_mutex_lock(lock);
        --thread_counter;
        pthread_mutex_unlock(lock);
    }


    if(root->index == 0)
        free(distances);

    return NULL;
}

//-----------------Helper Definitions-----------------//
double* select_distance_array(Index index) {
    Size div = 1 << (int)log2(index+1);
    return distances+(((index+1) % div) * (d_size/div));
}

typedef struct d_params {
    double* local;
    Data data;
    Index left;
    Index right;
} DParams;

void* calculate_some_distances(void* args) {
    const DParams* const params = (DParams*)args;
    const Index left = params->left;
    const Index right = params->right;
    const Data data = params->data;
    double* const local = params->local;
    for(Index i = left; i <= right; ++i){
        double value = 0.0;
        for(Index j=0; j<data.dim; ++j) {
            double xj = data.points[IDX(i,j,data.dim)] - data.vantage[j];
            value += xj*xj;
        }
        local[i] = sqrt(value);
    }
    return NULL;
}

void calculate_parallel_distances(double* local, Data data, pthread_mutex_t* lock) {
    DParams params[MAX_THREADS];
    Index id[MAX_THREADS];
    const Size div = data.size/LEAST_SIZE;
    const Size rem = data.size%LEAST_SIZE;
    Index i;
    Size live = 0;

    pthread_mutex_lock(lock);
    for(i = 0; i < div && thread_counter < MAX_THREADS; ++i) {
        params[live] = (DParams){.local=local, .data=data, .left=i*LEAST_SIZE, .right=(i+1)*LEAST_SIZE-1};
        if (pthread_create(id+(live), NULL, calculate_some_distances, (void*)(params+(live))) == 0) {
            live++;
            thread_counter++;
        } else {
            throw(PT_ERR_MESSAGE);
        }
    }
    pthread_mutex_unlock(lock);
    for(;i<div; ++i) {
        DParams params = (DParams){.local=local, .data=data, .left=i*LEAST_SIZE, .right=(i+1)*LEAST_SIZE-1};
        calculate_some_distances((void*)&params);
    }
    if(rem) {
        DParams params = (DParams){.local=local, .data=data, .left=i*LEAST_SIZE, .right=i*LEAST_SIZE+rem-1};
        calculate_some_distances((void*)&params);
    }

    for(i=0; i<live; ++i) {
        pthread_join(id[i], NULL);
        pthread_mutex_lock(lock);
        thread_counter--;
        pthread_mutex_unlock(lock);
    }
}

double kth_nearest(Node* root, const Index k, pthread_mutex_t* lock) {
    const Data data = root->data;
    double* const local = select_distance_array(root->index);
    calculate_parallel_distances(local, data, lock);
    return quick_select(data, local, 0, data.size-1, k);
}
//-----------------EOF-----------------//