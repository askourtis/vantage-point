#include <pthreads.h>
#include <vptree.h>
#include <algorithms.h>
#include <assert.h>


//-----------------Definitions-----------------//
vptree* buildvp(double* points, int size, int dim) {
    vptree* root = (vptree*)calloc(next_power_of_two(size)-1, sizeof(vptree));
    if(root == NULL)
        throw(MEM_ERR_MESSAGE);
    root->data=(Data){.points=points, .vantage=points+(IDX(size-1, 0, dim)), .size=size-1, .dim=dim};
    pthread_mutex_t lock;
    if(pthread_mutex_init(&lock, NULL) != 0)
        throw(MUT_ERR_MESSAGE);
    Params params = (Params){.root=root, .lock=&lock};
    build_tree((void*)&params);
    pthread_mutex_destroy(&lock);
    return root;
}

vptree* getInner(vptree* root) {
    assert(root!=NULL);
    vptree* ret = root+(root->index+1);
    if(root->value < 0)
        return NULL;
    return ret;
}

vptree* getOuter(vptree* root) {
    assert(root!=NULL);
    vptree* ret = root+(root->index+2);
    if(root->value < 0 || ret->data.vantage == NULL)
        return NULL;
    return ret;
}

double getMD(vptree* root) {
    assert(root!=NULL);
    return root->value;
}

double* getVP(vptree* root) {
    assert(root!=NULL);
    return root->data.vantage;
}

int getIDX(vptree* node) {
    assert(node!=NULL);
    vptree* root = node-node->index;
    return (node->data.vantage - root->data.points)/node->data.dim;
}
//-----------------EOF-----------------//