#include <algorithms.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>


//-----------------Declarations-----------------//
Index partition(const Data data, double* distances, const Index left, const Index right);
void swap(double* const first, double* const second);


//-----------------Definitions-----------------//
double quick_select(const Data data, double* const distances, const Index left, const Index right, const Index k) {
    assert(k>=0 && k<=right-left);
    Index index = partition(data, distances, left, right);
    if(k == index - left)
        return distances[index];
    if(k < index - left)
        return quick_select(data, distances, left, index-1, k);
    return quick_select(data, distances, index+1, right, k-index+left-1);
}


Size next_power_of_two(const Size n) {
    Size ret = n;
    ret |= ret >> 1;
    ret |= ret >> 2;
    ret |= ret >> 4;
    ret |= ret >> 8;
    ret |= ret >> 16;
    ret |= ret >> 32;
    ret++;
    return ret;
}

//-----------------Helper Definitions-----------------//
Index partition(const Data data, double* distances, const Index left, const Index right) {
    Index pivot_index = rand()%(right-left+1) + left;
    double pivot = distances[pivot_index];
    swap(distances+(pivot_index), distances+(right));
    for(Index k=0; k<data.dim; ++k)
        swap(data.points+(IDX(pivot_index,k,data.dim)), data.points+(IDX(right,k,data.dim)));
    Index i = left;
    for(Index j=left; j<right; ++j) {
        if(distances[j] <= pivot) {
            for(Index k=0; k<data.dim; ++k)
                swap(data.points+(IDX(i,k,data.dim)), data.points+(IDX(j,k,data.dim)));
            swap(distances+(i), distances+(j));
            i++;
        }
    }
    for(Index k=0; k<data.dim; ++k)
        swap(data.points+(IDX(i,k,data.dim)), data.points+(IDX(right,k,data.dim)));
    swap(distances+(i), distances+(right));
    return i;
}

void swap(double* const first, double* const second) {
    double tmp = *first;
    *first = *second;
    *second = tmp;
}
//-----------------EOF-----------------//