#include <sequential.h>
#include <math.h>
#include <algorithms.h>
#include <assert.h>

//-----------------Declarations-----------------//
double kth_nearest(const Data data, const Index k);

//-----------------Definitions-----------------//
void build_tree(Node* root) {
    assert(root != NULL);

    //Unpack needed variables
    Data data = root->data;

    if(data.size == 0)
        return;

    //Index of the mid point (point with median distance from vantage)
    Index k = data.size/2 + data.size%2 - 1;
    root->value = kth_nearest(root->data, k);

    Size inner_size = k + 1;
    Size outer_size = data.size - inner_size;

    //No if since the inner_size is always greater than 0
    {
        double* const points = data.points;
        Node* inner = root+(root->index+1);
        *inner=(Node){.index=2*root->index+1,
                      .value  = -1.0,
                      .data=(Data){.vantage = points+(IDX(inner_size-1, 0, data.dim)),
                                   .points  = points,
                                   .size    = inner_size-1,
                                   .dim     = data.dim}};
        build_tree(inner);
    }

    if(outer_size) {
        double* const points = data.points + IDX(inner_size, 0, data.dim);
        Node* outer = root+(root->index+2);
        *outer=(Node){.index=2*root->index+2,
                      .value  = -1.0,
                      .data=(Data){.vantage = points+(IDX(outer_size-1, 0, data.dim)),
                                   .points  = points,
                                   .size    = outer_size-1,
                                   .dim     = data.dim}};
        build_tree(outer);
    }
}

//-----------------Helper Definitions-----------------//
double kth_nearest(const Data data, const Index k) {
    double* distances = (double*)malloc(data.size * sizeof(double));
    if(distances == NULL)
        throw(MEM_ERR_MESSAGE);
    for(Index i=0; i<data.size; ++i) {
        double value = 0.0;
        for(Index j=0; j<data.dim; ++j) {
            double xj = data.points[IDX(i,j,data.dim)] - data.vantage[j];
            value += xj*xj;
        }
        distances[i] = sqrt(value);
    }
    double ret = quick_select(data, distances, 0, data.size-1, k);
    free(distances);
    return ret;
}
//-----------------EOF-----------------//